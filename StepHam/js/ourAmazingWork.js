let activeImages = 12;

$(".works-menu-item").on("click", aktivateWorks);

function aktivateWorks(Event) {
    activeImages = 12;
    let filterClass = $(this).attr("data-filter");
    $("#load-more").removeClass("hide");
    $("#worksMenu .active-works").removeClass("active-works");
    $(this).addClass("active-works");
    $(".works-pictures").children().addClass("hide");
    showPictures(filterClass, activeImages);
}

$(".btn-load").on("click", loadMore);

function loadMore(Event) {
    activeImages += 12;
    if (activeImages >= 36) {
        $("#load-more").addClass("hide");
    }
    let filterClass = $("#worksMenu .active-works").attr("data-filter");
    showPictures(filterClass, activeImages);

}




function showPictures(filterClass, activeImages) {
    if (filterClass == "all") {
        $(`.works-pictures .hide:lt(${activeImages})`).removeClass("hide");
    } else {
        $(`.${filterClass}:lt(${activeImages})`).removeClass("hide");
    }
}
