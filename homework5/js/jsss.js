let obj = {
    id: 123,
    name: 'Some',
    insideObj: {
        name: 'Inside',
        oneMoreObj: {
            name: 'oneMore',
            masiv2: ["123", "54", "sdljfn", "10756"]
        },
        masiv: ["123", "54", "sdljfn", "10756"]
    },
    end: true
};



function clone(o) {
    let cloneObJ = {};
    let cloneArr = [];
    let i = 0;
    if (Array.isArray(o)) {
        for (val of o) {
            cloneArr[i] = val;
            i++
        }
        return cloneArr;
    } else {
        for (key in o) {
            if (typeof (o[key]) === "object" && o[key] != null) {
                cloneObJ[key] = clone(o[key]);
            } else {
                cloneObJ[key] = o[key];
            }
        }
        return cloneObJ
    }
}
console.log(clone(obj));
console.log(obj);